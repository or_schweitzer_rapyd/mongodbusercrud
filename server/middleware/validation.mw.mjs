


import raw from "./route.async.wrapper.mjs"
import {createUserschema,updateUserschema} from "../modules/user/user.validation.mjs";

export async function validateCreateUser(req,res,next){
  const result =  createUserschema.validate(req.body);
  console.log(result);
  if(result.error){
    throw new Error(result.error.details[0].message);
  }

  next();
}

export async function validateUpdateUser(req,res,next){
  const result =  updateUserschema.validate(req.body);
  console.log(result);
  if(result.error){
    throw new Error(result.error.details[0].message);
  }
  next();

}

