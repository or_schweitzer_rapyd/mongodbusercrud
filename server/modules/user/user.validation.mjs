
import Joi from "joi";

export const createUserschema = Joi.object({
    first_name: Joi.string()
    .pattern(new RegExp('^[a-zA-Z]+'))
        .min(3)
        .max(30)
        .required(),

    last_name: Joi.string()
        .pattern(new RegExp('^[a-zA-Z]+'))
            .min(3)
            .max(30)
            .required(),

    email: Joi.string().email()
    .required(),

     phone: Joi.string()
     .pattern(new RegExp('[0-9]+'))
     .required()
    
});

export const updateUserschema = Joi.object({
    first_name: Joi.string()
    .pattern(new RegExp('^[a-zA-Z]+'))
        .min(3)
        .max(30)
        .optional(),

    last_name: Joi.string()
        .pattern(new RegExp('^[a-zA-Z]+'))
            .min(3)
            .max(30)
            .optional(),

    email: Joi.string().email(),

     phone: Joi.string()
     .pattern(new RegExp('[0-9]+'))
     .optional()
    
});






